import React                    from 'react';
import {Router, browserHistory} from 'react-router';
import {render}                 from 'react-dom';



export default routes => {

    const AppContainer = <Router history={browserHistory} children={routes} />;

    render(AppContainer, document.getElementById('app'));
    
}