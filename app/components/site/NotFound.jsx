import React from 'react';

export default class extends React.Component {

    render() {
        return (
            <div className="s-error">
                <h1>404: Page is not found...</h1>
            </div>
        )
    }

}