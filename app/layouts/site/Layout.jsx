import React from 'react';
import {Link} from 'react-router';

export default class extends React.Component {

    render() {
        return (
            <section className="s-layout">
                <ul>
                    <li><Link to="/" onlyActiveOnIndex>Home</Link></li>
                    <li><Link to="/about">Abouttt</Link></li>
                </ul>
                {this.props.children}
            </section>
        )
    }

}