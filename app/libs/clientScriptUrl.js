import config from '../../config/servers/_basic.js';

export default (asset, type) => {

    if (process.env.NODE_ENV !== 'production') {

        return `http://localhost:${config.devPort}/assets/${asset}.${type}`;

    } else {

        const assets = require('./public/assets/manifest.json');

        return `/assets/${assets[`${asset}.${type}`]}`;

    }

}
