import React    from 'react';
import {Route}  from 'react-router';

import Site     from '../layouts/site/Layout';
import Home     from '../components/site/Home';
import About    from '../components/site/About';
import NotFound from '../components/site/NotFound';

export default (
    <Route name="site" component={Site}>
        <Route name="home"     path="/"      component={Home}     />
        <Route name="about"    path="/about" component={About}    />
        <Route name="notFound" path="*"      component={NotFound} />
    </Route>
)