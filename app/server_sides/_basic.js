import Pug from 'pug';
import React from 'react';
import { renderToString } from 'react-dom/server';
import { match, RouterContext } from 'react-router';

export default (req, res, next, {context, routes, templateLocals}) => {
    
    match({ routes, location: req.url }, (err, redirect, props) => {

        if (err) {

            res.status(500).send(err.message)

        } else if (redirect) {

            res.redirect(redirect.pathname + redirect.search)

        } else if (props) {

            templateLocals.body = renderToString(<RouterContext {...props}/>);
            const html = Pug.renderFile(`./app/layouts/${context}/Layout.pug`, templateLocals, null);
            res.send(html);

        } else {

            res.status(404).send('Not Found')

        }

    })
    
}