import clientScriptUrl from '../libs/clientScriptUrl';
import routes from '../routes/site';
import basicMiddleware from './_basic';

const context = 'site';

export default (req, res, next) => {

    const params = {

        routes,
        context,
        templateLocals: {
            jsAsset: clientScriptUrl(context, 'js'),
            commonJsAsset: clientScriptUrl('common', 'js')
        }

    };

    basicMiddleware(req, res, next, params);

}