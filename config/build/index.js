import webpackServers from './webpack.servers';
import webpackProd    from './webpack.prod';
import webpackDev     from './webpack.dev';

const contexts = ['site'];

export default {

    webpackServers: webpackServers(contexts),

    webpackDev: webpackDev(contexts),

    webpackProd: webpackProd(contexts),

    contexts

};