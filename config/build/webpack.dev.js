import webpack from 'webpack';
import path from 'path';
import serverConfig from '../servers/_basic';

export default contexts => {

    let entries = {common: [
        'react',
        'react-router'
    ]};
    contexts.forEach(context => {
        entries[context] = [
            `webpack-dev-server/client?http://localhost:${serverConfig.devPort}/`,
            'webpack/hot/dev-server',
            `./app/client_sides/${context}.js`
        ];
    });

    return {

        entry: entries,

        output: {
            path: path.join(process.cwd(), 'public', 'assets'),
            publicPath: `http://localhost:${serverConfig.devPort}/assets/`,
            filename: '[name].js',
        },

        resolve: {
            extensions: ['', '.js', '.jsx']
        },

        devtool: 'inline-source-map',

        module: {
            loaders: [
                {
                    test: /(\.js|\.jsx)$/,
                    exclude: /node_modules/,
                    loaders: ['react-hot', 'babel']
                }
            ]
        },

        plugins: [
            new webpack.HotModuleReplacementPlugin(),
            new webpack.DefinePlugin({
                '__DEV__': true
            }),
            new webpack.optimize.CommonsChunkPlugin({
                name: 'common',
                chunks: ['site'],
                filename: 'common.js',
                minChunks: Infinity
            })
        ]

    }

};