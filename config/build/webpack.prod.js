import webpack  from 'webpack';
import Manifest from 'webpack-manifest-plugin';

export default contexts => {

    let entries = {};
    contexts.forEach(context => {
        entries[context] = `./app/client_sides/${context}.js`;
    });

    return {

        entry: entries,

        output: {
            path: './public/assets',
            filename: '[name].[chunkhash].js'
        },

        resolve: {
            extensions: ['', '.js', '.jsx']
        },

        devtool: false,
        debug: false,
        progress: true,
        node: {
            fs: 'empty'
        },

        module: {
            loaders: [
                {
                    test: /(\.js|\.jsx)$/,
                    exclude: /node_modules/,
                    loader: 'babel-loader'
                }
            ]
        },

        plugins: [
            new Manifest(),
            new webpack.optimize.CommonsChunkPlugin({
                name: 'common',
                filename: 'common.[chunkhash].js',
                minChunks: Infinity
            })
        ]

    }

};