import fs from 'fs';

export default contexts => {

    let entries = {
        dev: './servers/_dev.js'
    };
    contexts.forEach(context => {
        entries[context] = `./servers/${context}.js`;
    });

    const nodeExternals = fs.readdirSync('./node_modules')
        .concat([
            'react-dom/server',
            './public/assets/manifest.json'
        ])
        .reduce(function (ext, mod) {
            ext[mod] = 'commonjs ' + mod;
            return ext;
        }, {});

    return {

        entry: entries,

        output: {
            filename: 'server.[name].js'
        },

        resolve: {
            extensions: ['', '.js', '.jsx']
        },

        target: 'node',

        node: {
            __filename: false,
            __dirname: false
        },

        externals: nodeExternals,

        module: {
            loaders: [
                {
                    test: /(\.js|\.jsx)$/,
                    exclude: /node_modules/,
                    loader: 'babel-loader'
                }
            ]
        }

    };

}