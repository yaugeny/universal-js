const config = {};

config.env     = process.env.NODE_ENV  || 'development';
config.devPort = 3001;

export default config;