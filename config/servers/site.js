import config from './_basic';

config.context = 'site';
config.appPort = process.env.APP_PORT || 3000;

export default config;