import gulp     from 'gulp';
import webpack  from 'webpack';
import {exec}   from 'child_process';
import gutil    from 'gulp-util';
import run      from 'run-sequence';
import del      from 'del';

import config   from './config/build';


const isDevBuild = process.argv.indexOf('start:dev') !== -1;
const bundlesConfig = isDevBuild ? config.webpackDev : config.webpackProd;
const serversConfig = config.webpackServers;


gulp.task('build', done => {
    run(['clean'], ['servers'], ['bundles'], done);
});

gulp.task('start:dev', done => {
    run(['clean'], ['servers'], ['bundles'], ['http'], done);
});

gulp.task('start:prod', done => {
    run(['clean'], ['servers'], ['bundles'], ['http'], done);
});


gulp.task('clean', done => {

    del(['./public/**/*', './server.*.js']).then(() => {
        done();
    });

});


gulp.task('servers', (done) => {

    webpack(serversConfig).run(handleWebpackBuild(done));

});


gulp.task('bundles', done => {

    if (isDevBuild) {

        startServer('server.dev.js', done);

    } else {

        webpack(bundlesConfig).run(handleWebpackBuild(done));

    }

});


gulp.task('http', done => {

    const contexts = config.contexts;
    let i = contexts.length;

    contexts.forEach(context => {
        startServer(`server.${context}.js`);
        if (--i === 0) done();
    })

});


const startServer = (serverPath, done) => {

    const prodFlag = !isDevBuild ? 'NODE_ENV=production' : '';
    const server = exec(`NODE_PATH=. ${prodFlag} node ${serverPath}`);

    server.stdout.on('data', data => {
        if (done && data === 'Webpack: Done!') {
            done();
        } else {
            gutil.log(data.trim());
        }
    });

    server.stderr.on('data', data => {
        gutil.log(
            gutil.colors.yellow(
                data.trim()
            )
        );
        gutil.beep();
    });

};


const handleWebpackBuild = done => {

    return (err, stats) => {

        if (err) {
            return gutil.log(
                gutil.colors.yellow(err)
            );
        }

        const jsonStats = stats.toJson();

        if (jsonStats.errors.length > 0) {
            return gutil.log(
                gutil.colors.yellow(
                    jsonStats.errors
                )
            );
        }

        if (jsonStats.warnings.length > 0) {
            gutil.log(
                gutil.colors.yellow(
                    jsonStats.warnings
                )
            );
        }

        done();

    }

};