/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var _webpack = __webpack_require__(1);

	var _webpack2 = _interopRequireDefault(_webpack);

	var _webpackDevServer = __webpack_require__(2);

	var _webpackDevServer2 = _interopRequireDefault(_webpackDevServer);

	var _build = __webpack_require__(3);

	var _build2 = _interopRequireDefault(_build);

	var _basic = __webpack_require__(10);

	var _basic2 = _interopRequireDefault(_basic);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var webpackConfig = _build2.default.webpackDev;
	var compiler = (0, _webpack2.default)(webpackConfig);

	var initialCompile = true;

	compiler.plugin('done', function () {
	    if (initialCompile) {
	        initialCompile = false;
	        process.stdout.write('Webpack: Done!');
	    }
	});

	var devServer = new _webpackDevServer2.default(compiler, {
	    contentBase: 'http://localhost:' + _basic2.default.devPort,
	    publicPath: webpackConfig.output.publicPath,
	    hot: true,
	    inline: true,
	    historyApiFallback: true,
	    quiet: false,
	    noInfo: false,
	    lazy: false,
	    stats: {
	        colors: true,
	        hash: false,
	        version: false,
	        chunks: false,
	        children: false
	    }
	});

	devServer.listen(_basic2.default.devPort, 'localhost', function (err) {
	    if (err) console.error(err);
	    console.log('Webpack development server is running on port ' + _basic2.default.devPort);
	});

/***/ },
/* 1 */
/***/ function(module, exports) {

	module.exports = require("webpack");

/***/ },
/* 2 */
/***/ function(module, exports) {

	module.exports = require("webpack-dev-server");

/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _webpack = __webpack_require__(4);

	var _webpack2 = _interopRequireDefault(_webpack);

	var _webpack3 = __webpack_require__(6);

	var _webpack4 = _interopRequireDefault(_webpack3);

	var _webpack5 = __webpack_require__(8);

	var _webpack6 = _interopRequireDefault(_webpack5);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var contexts = ['site'];

	exports.default = {

	    webpackServers: (0, _webpack2.default)(contexts),

	    webpackDev: (0, _webpack6.default)(contexts),

	    webpackProd: (0, _webpack4.default)(contexts),

	    contexts: contexts

	};

/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _fs = __webpack_require__(5);

	var _fs2 = _interopRequireDefault(_fs);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	exports.default = function (contexts) {

	    var entries = {
	        dev: './servers/_dev.js'
	    };
	    contexts.forEach(function (context) {
	        entries[context] = './servers/' + context + '.js';
	    });

	    var nodeExternals = _fs2.default.readdirSync('./node_modules').concat(['react-dom/server', './public/assets/manifest.json']).reduce(function (ext, mod) {
	        ext[mod] = 'commonjs ' + mod;
	        return ext;
	    }, {});

	    return {

	        entry: entries,

	        output: {
	            filename: 'server.[name].js'
	        },

	        resolve: {
	            extensions: ['', '.js', '.jsx']
	        },

	        target: 'node',

	        node: {
	            __filename: false,
	            __dirname: false
	        },

	        externals: nodeExternals,

	        module: {
	            loaders: [{
	                test: /(\.js|\.jsx)$/,
	                exclude: /node_modules/,
	                loader: 'babel-loader'
	            }]
	        }

	    };
	};

/***/ },
/* 5 */
/***/ function(module, exports) {

	module.exports = require("fs");

/***/ },
/* 6 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _webpack = __webpack_require__(1);

	var _webpack2 = _interopRequireDefault(_webpack);

	var _webpackManifestPlugin = __webpack_require__(7);

	var _webpackManifestPlugin2 = _interopRequireDefault(_webpackManifestPlugin);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	exports.default = function (contexts) {

	    var entries = {};
	    contexts.forEach(function (context) {
	        entries[context] = './app/client_sides/' + context + '.js';
	    });

	    return {

	        entry: entries,

	        output: {
	            path: './public/assets',
	            filename: '[name].[chunkhash].js'
	        },

	        resolve: {
	            extensions: ['', '.js', '.jsx']
	        },

	        devtool: false,
	        debug: false,
	        progress: true,
	        node: {
	            fs: 'empty'
	        },

	        module: {
	            loaders: [{
	                test: /(\.js|\.jsx)$/,
	                exclude: /node_modules/,
	                loader: 'babel-loader'
	            }]
	        },

	        plugins: [new _webpackManifestPlugin2.default(), new _webpack2.default.optimize.CommonsChunkPlugin({
	            name: 'common',
	            filename: 'common.[chunkhash].js',
	            minChunks: Infinity
	        })]

	    };
	};

/***/ },
/* 7 */
/***/ function(module, exports) {

	module.exports = require("webpack-manifest-plugin");

/***/ },
/* 8 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _webpack = __webpack_require__(1);

	var _webpack2 = _interopRequireDefault(_webpack);

	var _path = __webpack_require__(9);

	var _path2 = _interopRequireDefault(_path);

	var _basic = __webpack_require__(10);

	var _basic2 = _interopRequireDefault(_basic);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	exports.default = function (contexts) {

	    var entries = { common: ['react', 'react-router'] };
	    contexts.forEach(function (context) {
	        entries[context] = ['webpack-dev-server/client?http://localhost:' + _basic2.default.devPort + '/', 'webpack/hot/dev-server', './app/client_sides/' + context + '.js'];
	    });

	    return {

	        entry: entries,

	        output: {
	            path: _path2.default.join(process.cwd(), 'public', 'assets'),
	            publicPath: 'http://localhost:' + _basic2.default.devPort + '/assets/',
	            filename: '[name].js'
	        },

	        resolve: {
	            extensions: ['', '.js', '.jsx']
	        },

	        devtool: 'inline-source-map',

	        module: {
	            loaders: [{
	                test: /(\.js|\.jsx)$/,
	                exclude: /node_modules/,
	                loaders: ['react-hot', 'babel']
	            }]
	        },

	        plugins: [new _webpack2.default.HotModuleReplacementPlugin(), new _webpack2.default.DefinePlugin({
	            '__DEV__': true
	        }), new _webpack2.default.optimize.CommonsChunkPlugin({
	            name: 'common',
	            chunks: ['site'],
	            filename: 'common.js',
	            minChunks: Infinity
	        })]

	    };
	};

/***/ },
/* 9 */
/***/ function(module, exports) {

	module.exports = require("path");

/***/ },
/* 10 */
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	var config = {};

	config.env = process.env.NODE_ENV || 'development';
	config.devPort = 3001;

	exports.default = config;

/***/ }
/******/ ]);