/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var _basic = __webpack_require__(11);

	var _basic2 = _interopRequireDefault(_basic);

	var _site = __webpack_require__(15);

	var _site2 = _interopRequireDefault(_site);

	var _site3 = __webpack_require__(28);

	var _site4 = _interopRequireDefault(_site3);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	(0, _basic2.default)(_site2.default, _site4.default);

/***/ },
/* 1 */,
/* 2 */,
/* 3 */,
/* 4 */,
/* 5 */,
/* 6 */,
/* 7 */,
/* 8 */,
/* 9 */
/***/ function(module, exports) {

	module.exports = require("path");

/***/ },
/* 10 */
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	var config = {};

	config.env = process.env.NODE_ENV || 'development';
	config.devPort = 3001;

	exports.default = config;

/***/ },
/* 11 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _express = __webpack_require__(12);

	var _express2 = _interopRequireDefault(_express);

	var _bodyParser = __webpack_require__(13);

	var _bodyParser2 = _interopRequireDefault(_bodyParser);

	var _cookieParser = __webpack_require__(14);

	var _cookieParser2 = _interopRequireDefault(_cookieParser);

	var _path = __webpack_require__(9);

	var _path2 = _interopRequireDefault(_path);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	exports.default = function (serverSideScript, config) {

	    var app = (0, _express2.default)();

	    app.listen(config.appPort, function (err) {
	        if (err) console.error(err);
	        console.log('⚡⚡⚡ Express server is running on port ' + config.appPort + ' ⚡⚡⚡');
	    });

	    app.use(_bodyParser2.default.json());

	    app.use(_bodyParser2.default.urlencoded({ extended: true }));

	    app.use((0, _cookieParser2.default)());

	    app.use(_express2.default.static(_path2.default.join(process.cwd(), 'public')));

	    app.use('/', serverSideScript);
	};

/***/ },
/* 12 */
/***/ function(module, exports) {

	module.exports = require("express");

/***/ },
/* 13 */
/***/ function(module, exports) {

	module.exports = require("body-parser");

/***/ },
/* 14 */
/***/ function(module, exports) {

	module.exports = require("cookie-parser");

/***/ },
/* 15 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _clientScriptUrl = __webpack_require__(16);

	var _clientScriptUrl2 = _interopRequireDefault(_clientScriptUrl);

	var _site = __webpack_require__(18);

	var _site2 = _interopRequireDefault(_site);

	var _basic = __webpack_require__(25);

	var _basic2 = _interopRequireDefault(_basic);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var context = 'site';

	exports.default = function (req, res, next) {

	    var params = {

	        routes: _site2.default,
	        context: context,
	        templateLocals: {
	            jsAsset: (0, _clientScriptUrl2.default)(context, 'js'),
	            commonJsAsset: (0, _clientScriptUrl2.default)('common', 'js')
	        }

	    };

	    (0, _basic2.default)(req, res, next, params);
	};

/***/ },
/* 16 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _basic = __webpack_require__(10);

	var _basic2 = _interopRequireDefault(_basic);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	exports.default = function (asset, type) {

	    if (process.env.NODE_ENV !== 'production') {

	        return 'http://localhost:' + _basic2.default.devPort + '/assets/' + asset + '.' + type;
	    } else {

	        var assets = __webpack_require__(17);

	        return '/assets/' + assets[asset + '.' + type];
	    }
	};

/***/ },
/* 17 */
/***/ function(module, exports) {

	module.exports = require("./public/assets/manifest.json");

/***/ },
/* 18 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _react = __webpack_require__(19);

	var _react2 = _interopRequireDefault(_react);

	var _reactRouter = __webpack_require__(20);

	var _Layout = __webpack_require__(21);

	var _Layout2 = _interopRequireDefault(_Layout);

	var _Home = __webpack_require__(22);

	var _Home2 = _interopRequireDefault(_Home);

	var _About = __webpack_require__(23);

	var _About2 = _interopRequireDefault(_About);

	var _NotFound = __webpack_require__(24);

	var _NotFound2 = _interopRequireDefault(_NotFound);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	exports.default = _react2.default.createElement(
	    _reactRouter.Route,
	    { name: 'site', component: _Layout2.default },
	    _react2.default.createElement(_reactRouter.Route, { name: 'home', path: '/', component: _Home2.default }),
	    _react2.default.createElement(_reactRouter.Route, { name: 'about', path: '/about', component: _About2.default }),
	    _react2.default.createElement(_reactRouter.Route, { name: 'notFound', path: '*', component: _NotFound2.default })
	);

/***/ },
/* 19 */
/***/ function(module, exports) {

	module.exports = require("react");

/***/ },
/* 20 */
/***/ function(module, exports) {

	module.exports = require("react-router");

/***/ },
/* 21 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	var _react = __webpack_require__(19);

	var _react2 = _interopRequireDefault(_react);

	var _reactRouter = __webpack_require__(20);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

	var _class = function (_React$Component) {
	    _inherits(_class, _React$Component);

	    function _class() {
	        _classCallCheck(this, _class);

	        return _possibleConstructorReturn(this, Object.getPrototypeOf(_class).apply(this, arguments));
	    }

	    _createClass(_class, [{
	        key: 'render',
	        value: function render() {
	            return _react2.default.createElement(
	                'section',
	                { className: 's-layout' },
	                _react2.default.createElement(
	                    'ul',
	                    null,
	                    _react2.default.createElement(
	                        'li',
	                        null,
	                        _react2.default.createElement(
	                            _reactRouter.Link,
	                            { to: '/', onlyActiveOnIndex: true },
	                            'Home'
	                        )
	                    ),
	                    _react2.default.createElement(
	                        'li',
	                        null,
	                        _react2.default.createElement(
	                            _reactRouter.Link,
	                            { to: '/about' },
	                            'Abouttt'
	                        )
	                    )
	                ),
	                this.props.children
	            );
	        }
	    }]);

	    return _class;
	}(_react2.default.Component);

	exports.default = _class;

/***/ },
/* 22 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	var _react = __webpack_require__(19);

	var _react2 = _interopRequireDefault(_react);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

	var _class = function (_React$Component) {
	    _inherits(_class, _React$Component);

	    function _class() {
	        _classCallCheck(this, _class);

	        return _possibleConstructorReturn(this, Object.getPrototypeOf(_class).apply(this, arguments));
	    }

	    _createClass(_class, [{
	        key: "render",
	        value: function render() {
	            return _react2.default.createElement(
	                "div",
	                { className: "s-home" },
	                _react2.default.createElement(
	                    "h1",
	                    null,
	                    "Here's homepaaage!!!"
	                )
	            );
	        }
	    }]);

	    return _class;
	}(_react2.default.Component);

	exports.default = _class;

/***/ },
/* 23 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	var _react = __webpack_require__(19);

	var _react2 = _interopRequireDefault(_react);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

	var _class = function (_React$Component) {
	    _inherits(_class, _React$Component);

	    function _class() {
	        _classCallCheck(this, _class);

	        return _possibleConstructorReturn(this, Object.getPrototypeOf(_class).apply(this, arguments));
	    }

	    _createClass(_class, [{
	        key: "render",
	        value: function render() {
	            return _react2.default.createElement(
	                "div",
	                { className: "s-about" },
	                _react2.default.createElement(
	                    "h1",
	                    null,
	                    "Here's abooout!!"
	                )
	            );
	        }
	    }]);

	    return _class;
	}(_react2.default.Component);

	exports.default = _class;

/***/ },
/* 24 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	var _react = __webpack_require__(19);

	var _react2 = _interopRequireDefault(_react);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

	var _class = function (_React$Component) {
	    _inherits(_class, _React$Component);

	    function _class() {
	        _classCallCheck(this, _class);

	        return _possibleConstructorReturn(this, Object.getPrototypeOf(_class).apply(this, arguments));
	    }

	    _createClass(_class, [{
	        key: "render",
	        value: function render() {
	            return _react2.default.createElement(
	                "div",
	                { className: "s-error" },
	                _react2.default.createElement(
	                    "h1",
	                    null,
	                    "404: Page is not found..."
	                )
	            );
	        }
	    }]);

	    return _class;
	}(_react2.default.Component);

	exports.default = _class;

/***/ },
/* 25 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _pug = __webpack_require__(26);

	var _pug2 = _interopRequireDefault(_pug);

	var _react = __webpack_require__(19);

	var _react2 = _interopRequireDefault(_react);

	var _server = __webpack_require__(27);

	var _reactRouter = __webpack_require__(20);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	exports.default = function (req, res, next, _ref) {
	    var context = _ref.context;
	    var routes = _ref.routes;
	    var templateLocals = _ref.templateLocals;


	    (0, _reactRouter.match)({ routes: routes, location: req.url }, function (err, redirect, props) {

	        if (err) {

	            res.status(500).send(err.message);
	        } else if (redirect) {

	            res.redirect(redirect.pathname + redirect.search);
	        } else if (props) {

	            templateLocals.body = (0, _server.renderToString)(_react2.default.createElement(_reactRouter.RouterContext, props));
	            var html = _pug2.default.renderFile('./app/layouts/' + context + '/Layout.pug', templateLocals, null);
	            res.send(html);
	        } else {

	            res.status(404).send('Not Found');
	        }
	    });
	};

/***/ },
/* 26 */
/***/ function(module, exports) {

	module.exports = require("pug");

/***/ },
/* 27 */
/***/ function(module, exports) {

	module.exports = require("react-dom/server");

/***/ },
/* 28 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _basic = __webpack_require__(10);

	var _basic2 = _interopRequireDefault(_basic);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	_basic2.default.context = 'site';
	_basic2.default.appPort = process.env.APP_PORT || 3000;

	exports.default = _basic2.default;

/***/ }
/******/ ]);