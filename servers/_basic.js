import express  from 'express';
import parser   from 'body-parser';
import cookies  from 'cookie-parser';
import path     from 'path';

export default (serverSideScript, config) => {
    
    const app = express();

    app.listen(config.appPort, err => {
        if (err) console.error(err);
        console.log(`⚡⚡⚡ Express server is running on port ${config.appPort} ⚡⚡⚡`);
    });

    app.use(parser.json());

    app.use(parser.urlencoded({ extended: true }));

    app.use(cookies());

    app.use(express.static(path.join(process.cwd(), 'public')));

    app.use('/', serverSideScript);
    
}
    
