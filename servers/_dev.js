import webpack from 'webpack';
import WebpackDevServer from 'webpack-dev-server';
import buildConfig  from '../config/build';
import serverConfig from '../config/servers/_basic';

const webpackConfig = buildConfig.webpackDev;
const compiler = webpack(webpackConfig);

let initialCompile = true;

compiler.plugin('done', () => {
    if (initialCompile) {
        initialCompile = false;
        process.stdout.write('Webpack: Done!');
    }
});

const devServer = new WebpackDevServer(compiler, {
    contentBase       : `http://localhost:${serverConfig.devPort}`,
    publicPath        : webpackConfig.output.publicPath,
    hot               : true,
    inline            : true,
    historyApiFallback: true,
    quiet             : false,
    noInfo            : false,
    lazy              : false,
    stats             : {
        colors  : true,
        hash    : false,
        version : false,
        chunks  : false,
        children: false
    }
});

devServer.listen(serverConfig.devPort, 'localhost', function(err) {
    if (err) console.error(err);
    console.log(`Webpack development server is running on port ${serverConfig.devPort}`);
});