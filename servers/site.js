import startServer from './_basic.js'
import serverSideScript from '../app/server_sides/site.js'
import config from '../config/servers/site.js'

startServer(serverSideScript, config);